<div align="center">
  <h1>Command</h1>
</div>

<div align="center">
  <img src="command_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Command is a behavioral pattern that encapsulates a request, which enables batching, queueing
and undo operations.**

### Real-World Analogy

_Restaurant orders._

The waiter (invoker) writes the customer's (client) order on a piece of paper (command) and takes it to the chef
(receiver) to prepare the meal.

### Participants

- :bust_in_silhouette: **Command**
    - Defines an interface to:
        - `execute` the command.
- :man: **ConcreteCommand**
    - Stores a reference to the receiver.
    - Stores any data required to execute the command.
    - Provides an implementation for:
        - `execute`
- :man: **Invoker** (aka Sender)
    - asks the command to carry out the request
- :man: **Receiver**
    - Knows how to perform the operations associated with carrying out a request.
    - Any class may serve as receiver.

### Collaborations

The client configures a **ConcreteCommand** with a **Receiver**. The **Invoker** calls execute on the **Command**. And
the **ConcreteCommand** object uses its **Receiver** to do the work.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **When a request can be issued from different or unknown senders, changed dynamically, composed
into larger requests, undone, stored, send over the network, queued, or scheduled for later.**

### Motivation

- How can our GUI interact with our system, without knowing the receiver of the request or the operations that
  will carry it out?
- How can we replace requests performed by a GUI button dynamically, for example, when implementing context-sensitive
  menus?
- How can we support command scripting, by composing commands into a larger one?
- How can we easily support undo functionality for requests?
- How can we queue requests, to run them in separate threads?
- How can we delay or execution of the issued requests, and run them at night, when the servers aren't busy?

### Known Uses

- Driver port of Hexagonal Architecture
    - Driver Ports offer the application functionality to the outside world.
    - If we want to follow the Single Responsibility Principle, then we would have a lot of ports, one for each use
      case.
    - Instead, we might create a port that can handle Commands with a receiver for each type of command.
- GUI Operations:
    - The same action could be triggered by a button, menu item, or keyboard shortcut.
    - Implementing the action using the command pattern saves on code duplication
- database Transaction Management:
    - implement transactions by encapsulating a sequence of operations into a single command.
    - This allows for easy undo/redo functionality or implementing commit/rollback mechanisms in databases.
- Queueing and Task Scheduling:
    - Commands can be stored in queues, allowing for the delayed execution of operations or scheduling tasks at specific
      times.
- Macro Recording and Playback:
    - To support macro functionality, which allows users to record a sequence of actions and play them back later.
- Remote Execution:
    - Commands can be serialized and sent across a network, enabling remote execution.
- Remote Controls in Smart Homes:
    - Allow users to configure buttons on the remote control with commands
    - Like turning on/off lights, adjusting room temperature, or activating security systems.
- Automated Testing:
    - Commands can represent test cases or steps, each command object encapsulates a specific test action.
- Bank transactions
    - Transactions like withdraw, deposit and transfer can be represented as commands allowing undo and batching.
    - Or to store all transactions, to leave an audit trail.

### Categorization

Purpose:  **Behavioral**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Behavioral patterns, in general, are concerned with the assignment of responsibilities between objects.
They’re not just patterns of objects, but also the patterns of communication between those objects.

Behavioral **object** patterns use composition to achieve cooperation between objects.
An important issue here is how these peer objects know about each other.

The command pattern is concerned with encapsulating behavior in an object and delegating requests.

### Aspects that can vary

- When and how a request is fulfilled.

### Solution to causes of redesign

- Dependence on specific operations.
    - Directly using a particular operation commits the client to a single way of satisfying the request.
    - Avoiding hard-coded requests, make it easier to change the way a request gets satisfied.

- Tight coupling.
    - Hard to understand: a lot of context needs to be known to understand a part of the system.
    - Hard to change: changing one class necessitates changing many other classes.
    - Hard to reuse in isolation: because classes depend on each other.

### Consequences

| Advantages                                                                                                                                              | Disadvantages                                                                                                                       |
|---------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Decouples invoker from receiver.** <br> decouples the object that invokes the operation from the one that knows how to perform it. | :x: **Complexity.** <br> code may become more complicated since you’re introducing a whole new layer between senders and receivers. |
| :heavy_check_mark: **Extendable.** <br> Introduce new commands into the app without changing existing client code.                                      |                                                                                                                                     |
| :heavy_check_mark: **Commands as first-class objects.** <br> Add methods like undo, compose them into composite commands, store them in a queue, etc    |                                                                                                                                     |

### Relations with Other Patterns

_Distinction from other patterns:_

- Chain of Responsibility, Command, Mediator and Observer address various ways of connecting senders and receivers of
  requests:
    - Chain of Responsibility passes a request sequentially along a dynamic chain of potential receivers until one of
      them handles it.
    - Command establishes unidirectional connections between senders and receivers.
    - Mediator eliminates direct connections between senders and receivers, forcing them to communicate indirectly via a
      mediator object.
    - Observer lets receivers dynamically subscribe to and unsubscribe from receiving requests.
- Command and Strategy may look similar because you can use both to parameterize an object with some action. However,
  they have very different intents.
    - You can use Command to convert any operation into an object. The conversion lets you defer execution of the
      operation, queue it, store the history of commands, send commands to remote services, etc.
    - On the other hand, Strategy usually describes different ways of doing the same thing, letting you swap these
      algorithms within a single context class.

_Combination with other patterns:_

- A Composite can be used to implement MacroCommands.
- A Memento can keep the state that the command requires to undo its effect.
- A command that must be copied before being placed on the history list acts as a Prototype.
- The command pattern can be combined with Chain of Responsibility:
    - Handlers in Chain of Responsibility can be implemented as Commands. In this case, you can execute a lot of
      different operations over the same context object, represented by a request.
    - In another approach, the request itself is a Command object. In this case, you can execute the same operation in a
      series of different contexts linked into a chain.
- You can treat Visitor as a powerful version of the Command pattern. Its objects can execute operations over various
  objects of different classes.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **An invoker object that uses the command to satisfy a request. A Command class that uses a
Receiver when being executed.**

### Structure

```mermaid
classDiagram
    class Command {
        <<interface>>
        + execute(): void
    }

    class ConcreteCommand {
        - receiver: Receiver
        + execute(): void
    }

    class Receiver {
        + action(): void
    }

    class Invoker {
        - command: Command
        + setCommand(command: Command): void
        + executeCommand(): void
    }

    Command <|.. ConcreteCommand: implements
    Invoker *-- Command: composes
    Receiver --* ConcreteCommand: composes
```

### Variations

_Command Intelligence:_

- **Dumb Commands**: Merely defines a binding between a receiver and the actions that carry out the request.
    - :heavy_check_mark: Extremely simple implementation.
    - :x: Dependant on receiver.
- **Smart Commands**: Implements everything itself without delegating to a receiver at all.
    - :heavy_check_mark: Independent of existing classes.
    - :heavy_check_mark: Can be used when no suitable receiver exists.
    - :heavy_check_mark: Can be used when the command knows its receiver implicitly. For example a command that creates
      an application window.
    - :x: Complex commands can become hard to maintain.

_Other Variants:_

- **Undo / Redo**: Command provides way to reverse it's execution.
    - :heavy_check_mark: The command pattern makes supporting this quite easy.
    - :x: Command needs to store additional state. For example, the arguments send to the receiver.
    - :x: Application needs to store a history list of commands that have been executed. Which can be traversed
      backwards or forwards on undo and redo.
- **Batch commands**: A concreteCommand subclass that simply executes a sequence of Commands.
    - :heavy_check_mark: Can be executed like any other command.
    - :x: Batch command has no explicit receiver, because the commands it sequences define their own receiver
- **Transaction-based system**: Instead of storing data directly, store the set of changes made to the data. The current
  state can be computed by executing all those changes.
    - :heavy_check_mark: Automatically produce an audit trail.
    - :heavy_check_mark: Recover from a crash by re-executing stored commands

### Implementation

In this example we apply the command pattern to a simple webshop to add products to, remove products from and checkout a
shopping cart.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Command](https://refactoring.guru/design-patterns/command)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ArjanCodes: Implementing Undo And Redo With The Command Design Pattern](https://youtu.be/FM71_a3txTo?si=wOy8KJGqpQCqvjnf)
- [ArjanCodes: How To Make The Command Pattern More Flexible With One Simple Change](https://youtu.be/rGu33Tk0tCM?si=mjuelEHXGJJpUGvg)
- [Geekific: The Command Pattern Explained and Implemented in Java ](https://youtu.be/UfGD60BYzPM?si=V55Lj829NKl8nrco)

<br>
<br>
